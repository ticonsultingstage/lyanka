<?php


session_start();

?>



<!DOCTYPE html>
<html lang="en">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lyannaj Kréyol : Galerie</title>

  <!-- PLUGINS CSS STYLE -->
  <link href="plugins/jquery-ui/jquery-ui.css" rel="stylesheet">
  <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="plugins/rs-plugin/css/settings.css" media="screen">
  <link rel="stylesheet" type="text/css" href="plugins/selectbox/select_option1.css">
  <link rel="stylesheet" type="text/css" href="plugins/owl-carousel/owl.carousel.css" media="screen">
  <link rel="stylesheet" type="text/css" href="plugins/isotope/jquery.fancybox.css">
  <link rel="stylesheet" type="text/css" href="plugins/isotope/isotope.css">

  <!-- GOOGLE FONT -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Dosis:400,300,600,700' rel='stylesheet' type='text/css'>

  <!-- CUSTOM CSS -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="css/default.css" id="option_color">

  <!-- Icons -->
  <link rel="shortcut icon" href="img/favicon.png">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="body-wrapper">


  <div class="main-wrapper">
    <!-- HEADER -->
    <header id="pageTop" class="header-wrapper">
      <!-- COLOR BAR -->
      <div class="container-fluid color-bar top-fixed clearfix">
        <div class="row">
          <div class="col-sm-1 col-xs-2 bg-color-1">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-2">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-3">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-4">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-5">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-6">fix bar</div>
          <div class="col-sm-1 bg-color-1 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-2 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-3 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-4 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-5 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-6 hidden-xs">fix bar</div>
        </div>
      </div>

      <!-- TOP INFO BAR -->
      <div class="top-info-bar bg-color-7 hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
              <ul class="list-inline topList">
                <li><i class="fa fa-phone bg-color-2" aria-hidden="true"></i> 0590.98.76.54</li>
              </ul>
            </div>
            <div class="col-sm-5">
              <ul class="list-inline functionList">

                 <?php
               if(!isset($_SESSION['id']) || empty($_SESSION['id'])){
                 ?>
                 <li><i class="fa fa-unlock-alt bg-color-5" aria-hidden="true"></i> <a href='inscription.php' >Mon compte</a><span>ou</span><a href='inscription.php'>Créer un compte</a></li>

                 <li class="cart-dropdown">
                  <a href="devis.php" class="bg-color-6 shop-cart" >
                    <i class="fa fa-shopping-basket " aria-hidden="true"></i>
                    <span class="badge bg-color-1"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li><i class="fa fa-shopping-basket " aria-hidden="true"></i></li>
                  </ul>
                </li>
              

              <?php
            }
            
              
              else{
               ?>
               <!-- logged in user information -->
               <li><a href="logout.php">Déconnexion</a></li>
               <li>Bienvenue <strong><?php echo $_SESSION['nom']; ?></strong></li>

               <?php
             }
             ?>

              </ul>
            </div>
          </div>
        </div>
      </div>

      <!-- NAVBAR -->
      <nav id="menuBar" class="navbar navbar-default lightHeader" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo-lyannaj.png"><!--<h3>Lyannaj kréyol</h3>--></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown singleDrop color-1">
                <a href="index.php" class="dropdown-toggle">
                  <i class="fa fa-home bg-color-1" aria-hidden="true"></i> <span class="">Accueil</span>
                </a>
               <ul class="dropdown-menu dropdown-menu-left ">
                  <li class=""><a href="#presentation">Présentation</a></li>
                  <li class=""><a href="#nospoints">Nos points forts</a></li>
                  <li class=""><a href="#noscar">Nos caractéristiques</a></li>
                  <li class=""><a href="#nospartenaires">Nos partenaires</a></li>
                </ul>
              </li>

              <li class="dropdown singleDrop color-10 ">
                <a href="prestation.php" class="dropdown-toggle">
                  <i class="fa fa-list-ul bg-color-10" aria-hidden="true"></i> <span>Prestations</span></a>
                <ul class="dropdown-menu dropdown-menu-left">
                    <li class=""><a href="prestation.php">Mes prestations</a></li>
                  

                </ul>
              </li>
              <li class="dropdown singleDrop color-3 active">
                <a href="galerie.php" class="dropdown-toggle">
                  <i class="fa fa-image bg-color-3" aria-hidden="true"></i>
                  <span>Galerie</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-left">
                  <li class=""><a href="#photos">Photos</a></li>
                  <li class=""><a href="#videos">Vidéos</a></li>
                </ul>
              </li>

                <li class="dropdown singleDrop color-4">
                  <a href="reservation.php" class="dropdown-toggle" >
                    <i class="fa fa-calendar-check-o bg-color-4" aria-hidden="true"></i>
                    <span>Réservation</span>
                  </a>
                </li>

                <li class="dropdown singleDrop color-5 ">
                  <a href="contact.php" class="dropdown-toggle" >
                    <i class="fa fa-envelope-o bg-color-5" aria-hidden="true"></i>
                    <span>Contact</span>
                  </a>


                </li>
              </ul>
            </li>

          </ul>
        </div>

        <div class="cart-dropdown">
          <a href="#" class="bg-color-6 shop-cart">
            <i class="fa fa-shopping-basket " aria-hidden="true"></i>
            <span class="badge bg-color-1"></span>
          </a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li><i class="fa fa-shopping-basket " aria-hidden="true"></i></li>
            <li>
              <a href="single-product.html">

              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <!-- PAGE TITLE SECTION-->
    <section class="pageTitleSection">
      <div class="container">
        <div class="pageTitleInfo">
          <h2>Galerie</h2>
          <ol class="breadcrumb">
            <li><a href="index.php">Accueil</a></li>
            <li class="active">Galerie</li>
          </ol>
        </div>
      </div>
    </section>

    <!-- MAIN SECTION -->
     <section class="mainContent full-width clearfix homeGallerySection" >
      <div class="container" id="photos">
        <div class="row">
          <div class="sectionTitle text-center">
            <h2><span class="shape shape-left bg-color-4"></span>
              <span>Photos</span><span class="shape shape-right bg-color-4"></span></h2>
            </div>

            <div class="mainContent full-width clearfix homeGallerySection">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="filter-container isotopeFilters">
                      <ul class="list-inline filter">
                        <li class="active"><a href="#" data-filter="*">Voir tout</a></li>
                        <li><a href="prestation1" data-filter=".prestation1">Prestation 1</a></li>
                        <li><a href="#" data-filter=".prestation2">Prestation 2</a></li>
                        <li><a href="#" data-filter=".prestation3">Prestation 3</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="row isotopeContainer">

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation1">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_1.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_1.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation1">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_2.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_2.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation1">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_3.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_3.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation1">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_4.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_4.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation1">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_5.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_5.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation2">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_6.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_6.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation2">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_7.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_7.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation2">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_8.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_8.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation3">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_3.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_3.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation3">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_4.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_4.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation3">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_5.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_5.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation3">
                    <article class="">
                      <figure>
                        <img src="img/home/home_gallery/gallery_sm_6.jpg" alt="image" class="img-rounded">
                        <div class="overlay-background">
                          <div class="inner"></div>
                        </div>
                        <div class="overlay">
                          <a class="fancybox-pop" rel="portfolio-1" href="img/home/home_gallery/gallery_lg_6.jpg">
                            <i class="fa fa-search-plus" aria-hidden="true"></i>
                          </a>
                        </div>
                      </figure>
                    </article>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    


   
        <div class="container" id=videos>
          <div class="row">
            <div class="sectionTitle text-center">
              <h2><span class="shape shape-left bg-color-4"></span>
                <span>Vidéos</span><span class="shape shape-right bg-color-4"></span></h2>
              
            </div>
            <div class="mainContent full-width clearfix homeGallerySection">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="filter-container isotopeFilters">
                      <ul class="list-inline filter">
                        <li class="active"><a href="#" data-filter="*">Voir tout</a></li>
                        <li><a href="prestation1" data-filter=".prestation1">Prestation 1</a></li>
                        <li><a href="#" data-filter=".prestation2">Prestation 2</a></li>
                        <li><a href="#" data-filter=".prestation3">Prestation 3</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="row isotopeContainer">

                  <div class="col-md-3 col-sm-6 col-xs-12 isotopeSelector prestation1">
                    <article class="">
                      <figure>
                       <div class="embed-responsive embed-responsive-4by3">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/frRYI54sUYk"></iframe>
                      </div>
                      <div class="overlay-background">
                        <div class="inner"></div>
                      </div>
                      <div class="overlay">
                        <a class="fancybox-pop" rel="portfolio-1" href="https://www.youtube.com/embed/frRYI54sUYk">
                          <i class="fa fa-search-plus" aria-hidden="true"></i>
                        </a>
                      </div>
                    </figure>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
      
    </section>

    <!-- FOOTER -->
    <footer>
      <!-- COLOR BAR -->
      <div class="container-fluid color-bar clearfix">
        <div class="row">
          <div class="col-sm-1 col-xs-2 bg-color-1">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-2">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-3">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-4">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-5">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-6">fix bar</div>
          <div class="col-sm-1 bg-color-1 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-2 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-3 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-4 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-5 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-6 hidden-xs">fix bar</div>
        </div>
      </div>
      <!-- FOOTER INFO AREA -->
      
      
      <!-- COPY RIGHT -->
      <div class="copyRight clearfix">
        <div class="container">
          <div class="row">
            <div class="col-sm-5 col-sm-push-7 col-xs-12">
              <ul class="list-inline">
                <li><a href="#" class="bg-color-1"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="bg-color-2"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#" class="bg-color-3"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#" class="bg-color-4"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                <li><a href="#" class="bg-color-5"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
              </ul>
            </div>
            <div class="col-sm-7 col-sm-pull-5 col-xs-12">
              <div class="copyRightText">
                <p>© 2018 Copyright Lyannaj Kréyol par la Team Gagnante.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <div class="scrolling">
    <a href="#pageTop" class="backToTop hidden-xs" id="backToTop"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
  </div>

  <!-- LOGIN MODAL -->


  <!-- CREATE ACCOUNT MODAL -->


  <script src="plugins/jquery/jquery-min.js"></script>
  <script src="plugins/jquery-ui/jquery-ui.js"></script>
  <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script src="plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
  <script src="plugins/owl-carousel/owl.carousel.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
  <script src="plugins/counter-up/jquery.counterup.min.js"></script>
  <script src="plugins/isotope/isotope.min.js"></script>
  <script src="plugins/isotope/jquery.fancybox.pack.js"></script>
  <script src="plugins/isotope/isotope-triger.js"></script>
  <script src="plugins/countdown/jquery.syotimer.js"></script>
  <script src="plugins/velocity/velocity.min.js"></script>
  <script src="plugins/smoothscroll/SmoothScroll.js"></script>
  <script src="js/custom.js"></script>
</body>
</html>

