<?php
session_start();

require("connect.php");

try {
	$conn = new PDO("mysql:dbname=".BASE.";host=".SERVER, USER, PASSWD);
    // set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	if(isset($_SESSION['id'])){
	$verif = $conn->prepare('SELECT * FROM clients WHERE id = :id');

	$verif->execute(array(':nom' => $_SESSION['id']));
	$row = $verif->fetch(PDO::FETCH_ASSOC);


	if(!empty($row['Mdp']) AND password_verify($_POST['password_old'],$row[ 'Mdp'])){

		if (!empty($_POST['password_1']) AND !empty($_POST['password_2']) AND $_POST['password_1']==$_POST['password_2']){
			$password=crypt($_POST["password_1"]);
			$sql ="UPDATE clients SET Mdp='".$password."' WHERE Nom='".$_SESSION['nom']."'";
			 $conn->exec($sql);
			$_SESSION['success'] = "Votre compte a été modifié avec succès !";
			header('location: prestationcompte.html');
		}

		else {

			$_SESSION['error'] = 'Les 2 mots de passe ne sont pas identiques.';
			header('location: modifprofil.php');

		} 
	}	
	else{
		$_SESSION['error'] = "Mot de passe incorrect" ;
		header('location: modifprofil.php');
	}
}
else{
	$_SESSION['error'] = "Vous êtes déconnecté" ;
		header('location: inscription.php');
}

	
}

catch(PDOException $e)
{
	echo $sql . "<br>" . $e->getMessage();
}

$conn = null;


?>