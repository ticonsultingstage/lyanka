<?php


session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lyannaj Kréyol : Inscription</title>

  <!-- PLUGINS CSS STYLE -->
  <link href="plugins/jquery-ui/jquery-ui.css" rel="stylesheet">
  <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="plugins/rs-plugin/css/settings.css" media="screen">
  <link rel="stylesheet" type="text/css" href="plugins/selectbox/select_option1.css">
  <link rel="stylesheet" type="text/css" href="plugins/owl-carousel/owl.carousel.css" media="screen">
  <link rel="stylesheet" type="text/css" href="plugins/isotope/jquery.fancybox.css">
  <link rel="stylesheet" type="text/css" href="plugins/isotope/isotope.css">

  <!-- GOOGLE FONT -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Dosis:400,300,600,700' rel='stylesheet' type='text/css'>

  <!-- CUSTOM CSS -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="css/default.css" id="option_color">

  <!-- Icons -->
  <link rel="shortcut icon" href="img/favicon.png">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="body-wrapper">


  <div class="main-wrapper">
    <!-- HEADER -->
    <header id="pageTop" class="header-wrapper">
      <!-- COLOR BAR -->
      <div class="container-fluid color-bar top-fixed clearfix">
        <div class="row">
          <div class="col-sm-1 col-xs-2 bg-color-1">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-2">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-3">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-4">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-5">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-6">fix bar</div>
          <div class="col-sm-1 bg-color-1 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-2 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-3 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-4 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-5 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-6 hidden-xs">fix bar</div>
        </div>
      </div>

      <!-- TOP INFO BAR -->
      <div class="top-info-bar bg-color-7 hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
              <ul class="list-inline topList">
                <li><i class="fa fa-phone bg-color-2" aria-hidden="true"></i> 0590.98.76.54</li>
              </ul>
            </div>
            <div class="col-sm-5">
              <ul class="list-inline functionList">
               <?php
               if(!isset($_SESSION['nom']) || empty($_SESSION['nom'])){
                 ?>
                 <li><i class="fa fa-unlock-alt bg-color-5" aria-hidden="true"></i> <a href='inscription.php' >Mon compte</a><span>ou</span><a href='inscription.php'>Créer un compte</a></li>

                 <li class="cart-dropdown">
                  <a href="devis.html" class="bg-color-6 shop-cart" >
                    <i class="fa fa-shopping-basket " aria-hidden="true"></i>
                    <span class="badge bg-color-1"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li><i class="fa fa-shopping-basket " aria-hidden="true"></i></li>
                  </ul>
                </li>
              

              <?php
            }
            
              
              else{
               ?>
               <!-- logged in user information -->
               <li><a href="logout.php">Déconnexion</a></li>
               <li>Bienvenue <strong><?php echo $_SESSION['nom']; ?></strong></li>

               <?php
             }
             ?>

              </ul>
            </div>
          </div>
        </div>
      </div>

      <!-- NAVBAR -->
      <nav id="menuBar" class="navbar navbar-default lightHeader" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
           <a class="navbar-brand" href="index.html"><img src="img/logo-lyannaj.png"><!--<h3>Lyannaj kréyol</h3>--></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="active dropdown singleDrop color-1">
                <a href="index.html" class="dropdown-toggle">
                  <i class="fa fa-home bg-color-1" aria-hidden="true"></i> <span class="active">Accueil</span>
                </a>
               <ul class="dropdown-menu dropdown-menu-left ">
                  <li class=""><a href="#presentation">Présentation</a></li>
                  <li class=""><a href="#nospoints">Nos points forts</a></li>
                  <li class=""><a href="#noscar">Nos caractéristiques</a></li>
                  <li class=""><a href="#nospartenaires">Nos partenaires</a></li>
                </ul>
              </li>

              <li class="dropdown singleDrop color-10">
                <a href="prestation.php" class="dropdown-toggle">
                <i class="fa fa-list-ul bg-color-10" aria-hidden="true"></i> <span>Prestations</span></a>
                <ul class="dropdown-menu dropdown-menu-left">
                  <li class=""><a href="prestation.php">Mes prestations</a></li>
                </ul>
              </li>

              <li class="dropdown singleDrop color-3 active">
                <a href="galerie.php" class="dropdown-toggle">
                  <i class="fa fa-image bg-color-3" aria-hidden="true"></i>
                  <span>Galerie</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-left">
                  <li class="active"> <a href="galerie.php">Photos</a>
                  </li>
                  <li class=""><a href="galerie.php"> Vidéos</a>
                  </li>
                </ul>
              </li>

                <li class="dropdown singleDrop color-4">
                  <a href="reservation.php" class="dropdown-toggle" >
                    <i class="fa fa-calendar-check-o bg-color-4" aria-hidden="true"></i>
                    <span>Réservation</span>
                  </a>

                </li>
                <li class="dropdown singleDrop color-5">
                  <a href="contact.php" class="dropdown-toggle" >
                    <i class="fa fa-envelope-o bg-color-5" aria-hidden="true"></i>
                    <span>Contact</span>
                  </a>


                </li>
              </ul>
            </li>

          </ul>
        </div>

        <div class="cart-dropdown">
          <a href="#" class="bg-color-2 shop-cart">
            <i class="fa fa-shopping-basket " aria-hidden="true"></i>
            <span class="badge bg-color-2"></span>
          </a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li><i class="fa fa-shopping-basket " aria-hidden="true"></i></li>
            <li>
              <a href="single-product.html">

              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>



    <!-- PAGE TITLE SECTION-->
    <section class="pageTitleSection">
      <div class="container">
       <div class="pageTitleInfo">
          <h2>Inscription ou Connexion</h2>
          <ol class="breadcrumb">
            <li><a href="index.html">Accueil</a></li>
            <li class="active">Inscription ou Connexion</li>
          </ol>
        </div>
      </div>
    </section>

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-12">
            <div class="panel panel-default formPanel">
              <div class="panel-heading bg-color-5 border-color-5">
                <h3 class="panel-title">Inscription</h3>
              </div>
              <div class="panel-body">
                <form action="enregistrement.php" method="POST" role="form">

                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Statut"  name="statut" pattern=".{2,30}" required="required">
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Dénomination" name="denomination" pattern=".{2,30}" required="required">
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Nom" name="nom" pattern=".{2,30}" required="required">
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Prénom" pattern=".{2,30}" name="prenom" pattern=".{2,30}" required="required">
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Adresse" name="adresse" required="required">
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Code postal" name="cp" required="required"/>
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Ville" name="ville" required="required"/>
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Téléphone fixe" name="fixe" >
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Téléphone mobile" name="mobile">
                  </div>
                  <div class="form-group formField">
                    <input type="text" class="form-control" placeholder="Mail" name="Email">
                  </div>
                  <div class="form-group formField">
                    <input type="password" class="form-control" placeholder="Mot de passe" name="MDP">
                  </div>
                  <div class="form-group formField">
                    <input type="submit" class="btn btn-primary btn-block bg-color-2 border-color-2" value="Envoyer">
                  </div>
                </form>
              </div>
            </div>
          </div>
          <section>
        <div class="content">
          <!-- notification message -->
          <?php if (isset($_SESSION['error'])) : ?>
            <div class="error success" >
              <h6>
                <?php 
                echo $_SESSION['error']; 
                unset($_SESSION['error']);
                ?>
              </h6>
            </div>
          <?php endif ?>
        </div>
      </section>
      <section>
        <div class="content">
          <!-- notification message -->
          <?php if (isset($_SESSION['success'])) : ?>
            <div class="error success" >
              <h6>
                <?php 
                echo $_SESSION['success']; 
                unset($_SESSION['success']);
                ?>
              </h6>
            </div>
          <?php endif ?>
        </div>
      </section>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="panel panel-default formPanel">
              <div class="panel-heading bg-color-5 border-color-5">
                <h3 class="panel-title">Je suis déjà client</h3>
              </div>
              <div class="panel-body">
                <form action="connexion.php" method="POST" role="form">
                  <div class="form-group formField">
                    <input type="text" class="form-control" name="Email" placeholder="Mail">
                  </div>
                  <div class="form-group formField">
                    <input type="password" class="form-control" name="MDP" placeholder="Mot de passe">
                  </div>
                  <div class="form-group formField">
                    <input type="submit" class="btn btn-primary btn-block bg-color-2 border-color-2" value="Connexion">
                  </div>
                  <div class="form-group formField">
                    <p class="help-block"><a href="#">Accéder à mon compte facebook</a></p>
                  </div>
                </form>
                  <div class="form-group formField">
                    <p class="help-block"><a href="#">Mot de passe oublié ?</a></p>
                  </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<!-- FOOTER -->
<footer>
      <!-- COLOR BAR -->
      <div class="container-fluid color-bar clearfix">
        <div class="row">
          <div class="col-sm-1 col-xs-2 bg-color-1">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-2">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-3">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-4">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-5">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-6">fix bar</div>
          <div class="col-sm-1 bg-color-1 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-2 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-3 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-4 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-5 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-6 hidden-xs">fix bar</div>
        </div>
      </div>
   
        <!-- COPY RIGHT -->
        <div class="copyRight clearfix">
          <div class="container">
            <div class="row">
              <div class="col-sm-5 col-sm-push-7 col-xs-12">
                <ul class="list-inline">
                  <li><a href="#" class="bg-color-1"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="bg-color-2"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="bg-color-3"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="bg-color-4"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="bg-color-5"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
              <div class="col-sm-7 col-sm-pull-5 col-xs-12">
                <div class="copyRightText">
                  <p>© 2018 Copyright Lyannaj Kréyol par la Team Gagnante. <a href="https://www.iamabdus.com/"></a>.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <div class="scrolling">
      <a href="#pageTop" class="backToTop hidden-xs" id="backToTop"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
    </div>

    <!-- CONNEXION -->
    <div class="modal fade customModal" id="loginModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="panel panel-default formPanel">
            <div class="panel-heading bg-color-1 border-color-1">
              <h3 class="panel-title">Se connecter</h3>
            </div>
            <div class="panel-body">
              <form action="connexion.php" method="POST" role="form">
                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Votre nom">
                </div>
                <div class="form-group formField">
                  <input type="password" class="form-control" placeholder="Mot de passe">
                </div>
                <div class="form-group formField">
                  <input type="submit" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Connexion">
                </div>
                <div class="form-group formField">
                  <p class="help-block"><a href="#">Mot de pase oublié?</a></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- CREER UN NOUVEAU COMPTE -->
    <div class="modal fade customModal" id="createAccount" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="panel panel-default formPanel">
            <div class="panel-heading bg-color-1 border-color-1">
              <h3 class="panel-title">Créer un compte</h3>
            </div>
            <div class="panel-body">
              <form action="#" method="POST" role="form">

                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Statut" required="required">
                </div>

                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="dénomination" required="required">
                </div>

                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Nom" required="required">
                </div>
                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Prénom" required="required">
                </div>


                <div class="form-group formField">
                  <!--<input type="text" class="form-control" placeholder="Ville">-->
                  <select class="form-control" id="ville" name="ville" required>
                    <option value="">Selectionnez votre ville</option>
                    <option value="97139">Abymes</option>
                    <option value="97121">Anse-Bertrand</option>
                    <option value="97122">Baie-Mahault</option>
                    <option value="97123">Baillif</option>
                    <option value="97100">Basse-Terre</option>
                    <option value="97125">Bouillante</option>
                    <option value="97130">Capesterre-Belle-Eau</option>
                    <option value="97124">Deshaies</option>
                    <option value="97190">Gosier</option>
                    <option value="97113">Gourbeyre</option>
                    <option value="97128">Goyave</option>
                    <option value="97127">La-Désirade</option>
                    <option value="97129">Lamentin</option>
                    <option value="97160">Le-Moule</option>
                    <option value="97111">Morne-à-L'eau</option>
                    <option value="97170">Petit-Bourg</option>
                    <option value="97131">Petit-Canal</option>
                    <option value="97110">Pointe-à-Pitre</option>
                    <option value="97116">Pointe-Noire</option>
                    <option value="97117">Port-Louis</option>
                    <option value="97120">Saint-Claude</option>
                    <option value="97180">Sainte-Anne</option>
                    <option value="97115">Sainte-Rose</option>
                    <option value="97118">Saint-François</option>
                    <option value="97114">Trois-Rivières</option>
                    <option value="97141">Vieux-Fort</option>
                    <option value="97119">Vieux-Habitants</option>
                  </select>

                </div>

                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Téléphone Fixe" id="domicile" class="form-control" pattern="0590?[0-9]{6}" required="required">
                </div>

                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Téléphone Mobile" id="domicile" class="form-control" pattern="0690?[0-9]{6}" required="required">
                </div>



                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Code postal" pattern="[0-9]{5}">
                </div>

                <div class="form-group formField">
                  <input type="email" id="email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required="required"/>
                </div>

                <div class="form-group formField">
                  <input type="password" class="form-control" placeholder="Mot de passe">
                </div>
                <div class="form-group formField">
                  <input type="password" class="form-control" placeholder="Confirmez mot de passe">
                </div>
                <div class="form-group formField">
                  <input type="submit" class="btn btn-primary btn-block bg-color-3 border-color-3" value="ENREGISTRER">
                </div>
                <div class="form-group formField">
                  <p class="help-block">Avez-vous déjà un compte? <a href="#">Se connecter</a></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="plugins/jquery/jquery-min.js"></script>
    <script src="plugins/jquery-ui/jquery-ui.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
    <script src="plugins/owl-carousel/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="plugins/counter-up/jquery.counterup.min.js"></script>
    <script src="plugins/isotope/isotope.min.js"></script>
    <script src="plugins/isotope/jquery.fancybox.pack.js"></script>
    <script src="plugins/isotope/isotope-triger.js"></script>
    <script src="plugins/countdown/jquery.syotimer.js"></script>
    <script src="plugins/velocity/velocity.min.js"></script>
    <script src="plugins/smoothscroll/SmoothScroll.js"></script>
    <script src="js/custom.js"></script>

    <script src="js/calendrier.js"></script>
  </body>
  </html>
