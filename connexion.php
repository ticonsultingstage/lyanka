<?php 
session_start();
require("connect.php");

try {
	$conn = new PDO("mysql:dbname=".BASE.";host=".SERVER, USER, PASSWD);
    // set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
	$verif = $conn->prepare('SELECT * FROM clients WHERE Email = :email');
	
	$verif->execute(array(':email' => $_POST['Email']));
	$row = $verif->fetch(PDO::FETCH_ASSOC);


	if(!empty($row['Mdp']) AND password_verify($_POST['MDP'],$row['Mdp'])){
		$_SESSION['nom'] = $row['Nom'];
		$_SESSION['id'] = $row['id'];
		$_SESSION['succes'] = 'Connecté(e)';

		echo $_SESSION['id'];
		header('location: prestationcompte.php');
		exit();

	}
	else{
		$_SESSION['error']='Votre mot de passe ou votre identifiant est incorrect';
		header('location: inscription.php');
	}
}

catch(PDOException $e)
{
	echo $sql . "<br>" . $e->getMessage();
}

$conn = null;

?>