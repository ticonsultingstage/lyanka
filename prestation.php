<?php


session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lyannaj Kréyol : Prestations</title>

  <!-- PLUGINS CSS STYLE -->
  <link href="plugins/jquery-ui/jquery-ui.css" rel="stylesheet">
  <link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="plugins/rs-plugin/css/settings.css" media="screen">
  <link rel="stylesheet" type="text/css" href="plugins/selectbox/select_option1.css">
  <link rel="stylesheet" type="text/css" href="plugins/owl-carousel/owl.carousel.css" media="screen">
  <link rel="stylesheet" type="text/css" href="plugins/isotope/jquery.fancybox.css">
  <link rel="stylesheet" type="text/css" href="plugins/isotope/isotope.css">

  <!-- GOOGLE FONT -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Dosis:400,300,600,700' rel='stylesheet' type='text/css'>

  <!-- CUSTOM CSS -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="css/default.css" id="option_color">

  <!-- Icons -->
  <link rel="shortcut icon" href="img/favicon.png">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="body-wrapper">


  <div class="main-wrapper">
    <!-- HEADER -->
    <header id="pageTop" class="header-wrapper">
      <!-- COLOR BAR -->
      <div class="container-fluid color-bar top-fixed clearfix">
        <div class="row">
          <div class="col-sm-1 col-xs-2 bg-color-1">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-2">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-3">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-4">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-5">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-6">fix bar</div>
          <div class="col-sm-1 bg-color-1 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-2 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-3 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-4 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-5 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-6 hidden-xs">fix bar</div>
        </div>
      </div>

      <!-- TOP INFO BAR -->
      <div class="top-info-bar bg-color-7 hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
              <ul class="list-inline topList">
                <li><i class="fa fa-phone bg-color-2" aria-hidden="true"></i> 0590.98.76.54</li>
              </ul>
            </div>
            <div class="col-sm-5">
              <ul class="list-inline functionList">

                <?php
               if(!isset($_SESSION['id']) || empty($_SESSION['nom'])){
                 ?>
                 <li><i class="fa fa-unlock-alt bg-color-5" aria-hidden="true"></i> <a href='inscription.php' >Mon compte</a><span>ou</span><a href='inscription.php'>Créer un compte</a></li>

                 <li class="cart-dropdown">
                  <a href="devis.php" class="bg-color-6 shop-cart" >
                    <i class="fa fa-shopping-basket " aria-hidden="true"></i>
                    <span class="badge bg-color-1"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li><i class="fa fa-shopping-basket " aria-hidden="true"></i></li>
                  </ul>
                </li>
              

              <?php
            }
            
              
              else{
               ?>
               <!-- logged in user information -->
               <li><a href="logout.php">Déconnexion</a></li>
               <li>Bienvenue <strong><?php echo $_SESSION['nom']; ?></strong></li>

               <?php
             }
             ?>

              </ul>
            </div>
          </div>
        </div>
      </div>

      <!-- NAVBAR -->
      <nav id="menuBar" class="navbar navbar-default lightHeader" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
           <a class="navbar-brand" href="index.php"><img src="img/logo-lyannaj.png"><!--<h3>Lyannaj kréyol</h3>--></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class=" dropdown singleDrop color-1">
                <a href="index.html" class="dropdown-toggle">
                  <i class="fa fa-home bg-color-1 " aria-hidden="true"></i> <span class="">Accueil</span>
                </a>
               <ul class="dropdown-menu dropdown-menu-left ">
                  <li class=""><a href="#presentation">Présentation</a></li>
                  <li class=""><a href="#nospoints">Nos points forts</a></li>
                  <li class=""><a href="#noscar">Nos caractéristiques</a></li>
                  <li class=""><a href="#nospartenaires">Nos partenaires</a></li>
                </ul>
              </li>

              <li class="dropdown singleDrop color-10 active">
                <a href="prestation.php" class="dropdown-toggle"><i class="fa fa-list-ul bg-color-10" aria-hidden="true"></i> <span>Prestations</span></a>
                <ul class="dropdown-menu dropdown-menu-left">
                  <li class=""><a href="prestation.php">Mes prestations</a></li>
                </ul>
              </li>

              <li class="dropdown singleDrop color-3">
                <a href="galerie.php" class="dropdown-toggle">
                  <i class="fa fa-image bg-color-3" aria-hidden="true"></i>
                  <span>Galerie</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-left">
                  <li class=""> <a href="galerie.php">Photos</a>
                  </li>
                  <li class=""><a href="galerie.php"> Vidéos</a>
                  </li>
                </ul>
              </li>

                <li class="dropdown singleDrop color-4">
                  <a href="reservation.php" class="dropdown-toggle" >
                    <i class="fa fa-calendar-check-o bg-color-4" aria-hidden="true"></i>
                    <span>Réservation</span>
                  </a>

                </li>
                <li class="dropdown singleDrop color-5 ">
                  <a href="contact.php" class="dropdown-toggle" >
                    <i class="fa fa-envelope-o bg-color-5" aria-hidden="true"></i>
                    <span>Contact</span>
                  </a>


                </li>
              </ul>
            </li>

          </ul>
        </div>

        <div class="cart-dropdown">
          <a href="#" class="bg-color-6 shop-cart">
            <i class="fa fa-shopping-basket " aria-hidden="true"></i>
            <span class="badge bg-color-1"></span>
          </a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li><i class="fa fa-shopping-basket " aria-hidden="true"></i></li>
            <li>
              <a href="single-product.html">

              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>


    <!-- PAGE TITLE SECTION-->
    <section class="pageTitleSection">
      <div class="container">
        <div class="pageTitleInfo">
          <h2>Prestations</h2>
          <ol class="breadcrumb">
            <li><a href="index.php">Accueil</a></li>
            <li class="active">Prestations</li>
          </ol>
        </div>
      </div>
    </section>

    <!-- MAIN SECTION -->
    <section class="mainContent full-width clearfix">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="media courseList couresListPage">
              <a class="media-left border-color-1" href="course-single-left-sidebar.html">
                <img class="media-object" src="img/home/courses/course-1.jpg" alt="Image">
                
              </a>
              <div class="media-body">
                <h3 class="media-heading"><a href="course-single-left-sidebar.html" class="color-1">Phasellus convallis eros.</a></h3>
                <ul class="list-inline">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>They must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a com plete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself.</p>
                <ul class="list-inline btn-yellow btnPart">
                  <li><a href="reservation.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Réserver</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Plus d'infos</a></li>
                </ul>
              </div>
            </div>
            <div class="media courseList couresListPage">
              <a class="media-left border-color-2" href="course-single-left-sidebar.html">
                <img class="media-object" src="img/home/courses/course-2.jpg" alt="Image">
                
              </a>
              <div class="media-body">
                <h3 class="media-heading"><a href="course-single-left-sidebar.html" class="color-2">Phasellus convallis eros.</a></h3>
                <ul class="list-inline">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>They must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a com plete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself.</p>
                <ul class="list-inline btn-green btnPart">
                  <li><a href="reservation.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Réserver</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Plus d'infos</a></li>
                </ul>
              </div>
            </div>
            <div class="media courseList couresListPage">
              <a class="media-left border-color-3" href="course-single-left-sidebar.html">
                <img class="media-object" src="img/home/courses/course-3.jpg" alt="Image">
                
              </a>
              <div class="media-body">
                <h3 class="media-heading"><a href="course-single-left-sidebar.html" class="color-3">Phasellus convallis eros.</a></h3>
                <ul class="list-inline">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>They must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a com plete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself.</p>
                <ul class="list-inline btn-red btnPart">
                  <li><a href="reservation.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Réserver</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Plus d'infos</a></li>
                </ul>
              </div>
            </div>
            <div class="media courseList couresListPage">
              <a class="media-left border-color-4" href="course-single-left-sidebar.html">
                <img class="media-object" src="img/home/courses/course-4.jpg" alt="Image">
                
              </a>
              <div class="media-body">
                <h3 class="media-heading"><a href="course-single-left-sidebar.html" class="color-4">Phasellus convallis eros.</a></h3>
                <ul class="list-inline">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>They must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a com plete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself.</p>
                <ul class="list-inline btn-sky btnPart">
                  <li><a href="reservation.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Réserver</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Plus d'infos</a></li>
                </ul>
              </div>
            </div>
            <div class="media courseList couresListPage">
              <a class="media-left border-color-5" href="course-single-left-sidebar.html">
                <img class="media-object" src="img/home/courses/course-1.jpg" alt="Image">
               
              </a>
              <div class="media-body">
                <h3 class="media-heading"><a href="course-single-left-sidebar.html" class="color-5">Phasellus convallis eros.</a></h3>
                <ul class="list-inline">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>They must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a com plete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself.</p>
                <ul class="list-inline btn-purple btnPart">
                  <li><a href="reservation.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Réserver</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Plus d'infos</a></li>
                </ul>
              </div>
            </div>
            <div class="media courseList couresListPage">
              <a class="media-left border-color-6" href="course-single-left-sidebar.html">
                <img class="media-object" src="img/home/courses/course-2.jpg" alt="Image">
                
              </a>
              <div class="media-body">
                <h3 class="media-heading"><a href="course-single-left-sidebar.html" class="color-6">Phasellus convallis eros.</a></h3>
                <ul class="list-inline">
                  <li><i class="fa fa-calendar-o" aria-hidden="true"></i>Age 2 to 4 Years</li>
                  <li><i class="fa fa-clock-o" aria-hidden="true"></i>9.00AM-11.00AM</li>
                </ul>
                <p>They must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a com plete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself.</p>
                <ul class="list-inline btn-pink btnPart">
                  <li><a href="reservation.html" class="btn btn-primary "><i class="fa fa-shopping-basket " aria-hidden="true"></i>Réserver</a></li>
                  <li><a href="course-single-left-sidebar.html" class="btn btn-link"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Plus d'infos</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="pagerArea text-center">
          <ul class="pager">
            <li class="prev"><a href="#"><i class="fa fa-arrow-left" aria-hidden="true"></i>Préc.</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li class="next"><a href="#">Suiv.<i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>
          </ul>
        </div>

      </div>
    </section>

<!-- FOOTER -->
    <footer>
      <!-- COLOR BAR -->
      <div class="container-fluid color-bar clearfix">
        <div class="row">
          <div class="col-sm-1 col-xs-2 bg-color-1">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-2">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-3">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-4">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-5">fix bar</div>
          <div class="col-sm-1 col-xs-2 bg-color-6">fix bar</div>
          <div class="col-sm-1 bg-color-1 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-2 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-3 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-4 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-5 hidden-xs">fix bar</div>
          <div class="col-sm-1 bg-color-6 hidden-xs">fix bar</div>
        </div>
      </div>
   
        <!-- COPY RIGHT -->
        <div class="copyRight clearfix">
          <div class="container">
            <div class="row">
              <div class="col-sm-5 col-sm-push-7 col-xs-12">
                <ul class="list-inline">
                  <li><a href="#" class="bg-color-1"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="bg-color-2"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="bg-color-3"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="bg-color-4"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="bg-color-5"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
              </div>
              <div class="col-sm-7 col-sm-pull-5 col-xs-12">
                <div class="copyRightText">
                  <p>© 2018 Copyright Lyannaj Kréyol par la Team Gagnante. <a href="https://www.iamabdus.com/"></a>.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <div class="scrolling">
      <a href="#pageTop" class="backToTop hidden-xs" id="backToTop"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
    </div>

    <!-- CONNEXION -->
    <div class="modal fade customModal" id="loginModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="panel panel-default formPanel">
            <div class="panel-heading bg-color-1 border-color-1">
              <h3 class="panel-title">Se connecter</h3>
            </div>
            <div class="panel-body">
              <form action="#" method="POST" role="form">
                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Votre nom">
                </div>
                <div class="form-group formField">
                  <input type="password" class="form-control" placeholder="Mot de passe">
                </div>
                <div class="form-group formField">
                  <input type="submit" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Connecter">
                </div>
                <div class="form-group formField">
                  <p class="help-block"><a href="#">Mot de pase oublié?</a></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- CREER UN NOUVEAU COMPTE -->
    <div class="modal fade customModal" id="createAccount" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="panel panel-default formPanel">
            <div class="panel-heading bg-color-1 border-color-1">
              <h3 class="panel-title">Créer un compte</h3>
            </div>
            <div class="panel-body">
              <form action="#" method="POST" role="form">
                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Nom">
                </div>
                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Prénom">
                </div>
                <div class="form-group formField">
                  <input type="text" class="form-control" placeholder="Téléphone">
                </div>
                <div class="form-group formField">
                  <input type="password" class="form-control" placeholder="Mot de passe">
                </div>
                <div class="form-group formField">
                  <input type="password" class="form-control" placeholder="Confirmez mot de passe">
                </div>
                <div class="form-group formField">
                  <input type="submit" class="btn btn-primary btn-block bg-color-3 border-color-3" value="ENREGISTRER">
                </div>
                <div class="form-group formField">
                  <p class="help-block">Avez-vous déjà un compte? <a href="#">Se connecter</a></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="plugins/jquery/jquery-min.js"></script>
    <script src="plugins/jquery-ui/jquery-ui.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
    <script src="plugins/owl-carousel/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="plugins/counter-up/jquery.counterup.min.js"></script>
    <script src="plugins/isotope/isotope.min.js"></script>
    <script src="plugins/isotope/jquery.fancybox.pack.js"></script>
    <script src="plugins/isotope/isotope-triger.js"></script>
    <script src="plugins/countdown/jquery.syotimer.js"></script>
    <script src="plugins/velocity/velocity.min.js"></script>
    <script src="plugins/smoothscroll/SmoothScroll.js"></script>
    <script src="js/custom.js"></script>

    <script src="js/calendrier.js"></script>
  </body>
  </html>